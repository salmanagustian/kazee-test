<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      $users = [];

      $faker = Faker\Factory::create();

      for ($i = 0; $i <= 5; $i++) {
         $users[$i] = [
            'name' => $faker->name(),
            'email' => $faker->unique()->safeEmail,
            'password' => Hash::make('123456'),
            'roles' => rand(1, 2),
            'address' => $faker->address(),
            'phone' => $faker->phoneNumber(),
         ];
      }

      User::insert($users);
   }
}
