<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="title" content="PT. Kazee Digital Indonesia">
    <meta name="description" content="Harnessing the power of big data and artificial intelligence to improve decision making across your organization">
    <meta name="keywords" content="Website Kazee, Kazee Digital Indonesia, Big Data">
    <meta name="author" content="Sal.">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=5">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <!-- Core -->
    <link rel="stylesheet" href="{{ asset('assets/layouts/backend/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/input-form-custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/button-custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/modal-custom.css') }}">

    @stack('css-libraries')

</head>
<body class="sb-nav-fixed">
    @include('layouts.navbar')

    <div id="layoutSidenav">
        @include('layouts.sidebar')

        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    @yield('main-content')
                </div>
            </main>

            @include('layouts.footer')
        </div>
    </div>

    <!-- Core -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

    <script src="{{ asset('assets/layouts/backend/js/scripts.js') }}"></script>
    <script src="{{ asset('js/helpers.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

    <!-- Sweetalert -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Bootstrap datepicker -->
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script> --}}

    <script>
        const APP_URL= {!! json_encode(url('/')) !!};
    </script>

    @stack('script-libraries')
    @yield('script')

</body>
</html>