<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
      <div class="sb-sidenav-menu">
         <div class="nav">
             <div class="sb-sidenav-menu-heading"></div>

               <a class="nav-link" href="{{ route('dashboard') }}">
                  <div class="sb-nav-link-icon"><i class="fa fa-desktop"></i></div>
                  Dashboard
               </a>
               <a class="nav-link" href="{{ route('users.index') }}">
                  <div class="sb-nav-link-icon"><i class="fa fa-users"></i></div>
                  Users
               </a>
         </div>
     </div>
    </nav>
</div>