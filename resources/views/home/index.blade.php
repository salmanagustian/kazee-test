@extends('layouts.index')

@section('main-content')

<div class="row">
    <div class="col-md-3">
        <div class="card" style="border:none; border-radius:1rem;">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                    <div>
                        <h4 style="color: #3399FD;">TOTAL USERS</h4>
                    </div>
                    <div>
                        <h4>{{ $totalUsers }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')

@endsection

@endsection