
## Setup 
Clone repository menggunakan SSH atau via HTTPS.

Jika sudah lakukan tahapan berikut di cmd / git bash dst:

```bash
 copy file .env.example menjadi .env
```

```bash
composer install / composer update
```

Jika sudah langkah selanjutnya adalah

```bash
php artisan config:cache
php artisan config:clear
```

Selanjutnya, untuk melakukan generate table jangan lupa setting .env (nama database, username dan password di sesuaikan)

```bash
php artisan migrate:refresh --seed
php artisan key:generate
php artisan serve atau php -S localhost:8000 -t public/
```
