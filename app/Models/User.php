<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
   use Notifiable;

   const ROLE_ADMIN    = 1;
   const ROLE_CUSTOMER = 2;

   protected $guarded = [];
   
   /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
   protected $hidden = [
      'password', 'remember_token',
   ];

   protected $cast = [
      'dob' => 'date'
   ];

   public function created_at()
   {
      return Carbon::parse($this->created_at)->translatedFormat('d F Y');
   }

   public function getRole($role)
   {
      if(self::ROLE_ADMIN == $role) return '<span class="badge badge-danger">ADMIN</span>';
      if(self::ROLE_CUSTOMER == $role) return '<span class="badge badge-primary">CUSTOMER</span>';
   }
}
