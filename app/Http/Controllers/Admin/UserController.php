<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Traits\Helpers\Helper;
use App\Traits\Response\ResponseJson;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
   use Helper, ResponseJson;

   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
      return view('user.index');
   }

   /**
    * Get Users data collection.
    *
    * @return collection
    */

   public function grid()
   {
      $query = User::latest();

      return DataTables::of($query)
         ->addIndexColumn()
         ->editColumn('name', function ($query) {
            return '
               <span>'.$query->name.'</span> <br>
               <span>'.$query->getRole($query->roles).'</span>
            ';
         })
         ->editColumn('phone', function ($query) {
            return $query->phone ?? '-';
         })
         ->editColumn('address', function ($query) {
            return $query->address ?? '-';
         })
         ->addColumn('action', function ($query) {
            return $this->getActionButton($query);
         })
         ->rawColumns(['action', 'name'])
         ->make(true);
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(UserRequest $request)
   {
      $validated = $request->validated();

      User::create($validated);

      return $this->sendResponseSuccess(__('response.success'));
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\User  $user
    * @return \Illuminate\Http\Response
    */
   public function edit(User $user)
   {
      return response()->json(['status' => true, 'data' => $user]);
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\User  $user
    * @return \Illuminate\Http\Response
    */
   public function update(UserRequest $request, User $user)
   {
      $validated = $request->validated();

      $user->update($validated);

      return $this->sendResponseSuccess(__('response.success-update'));
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\User  $user
    * @return \Illuminate\Http\Response
    */
   public function destroy(User $user)
   {
      $user->delete();

      return $this->sendResponseSuccess(__('response.success-delete'));
   }
}
