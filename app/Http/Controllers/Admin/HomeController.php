<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
   /**
    * Show to dashboard.
    *
    * @return \Illuminate\Http\Response
    */

   public function __invoke()
   {
      $totalUsers = User::count();

      return view('home.index', compact('totalUsers'));
   }
}
