<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = request()->isMethod('put') ? 'nullable' : 'sometimes|required';

        return [
            'name'     => 'required',
            'email'    => 'required|email|unique:users,email,'.$this->id,
            'password' => $required,
            'phone'    => 'nullable',
            'roles'    => 'required',
            'address'  => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => 'Mohon isi nama lengkap anda',
            'email.required'    => 'Mohon isi email anda',
            'email.unique'    => 'Email yang anda masukan sudah terdaftar',
            'email.email'       => 'Format email anda salah',
            'password.required' => 'Mohon isi password anda',
            'roles.required'    => 'Mohon isi role anda',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'password' => !empty($this->password) ? Hash::make($this->password) : ''
        ]);
    }
}
