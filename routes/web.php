<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::namespace('Admin')->group(function () {
   Route::get('/', 'HomeController')->name('dashboard');

   // Users
   Route::get('users/grid', 'UserController@grid');
   Route::resource('users', 'UserController')->except(['create', 'show']);
});
